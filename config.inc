<?php
$templatedir="templates";
$entryn="{$templatedir}/entry.txt";
$headn="{$templatedir}/header.txt";
$queuen="{$templatedir}/queue.txt";
$rssheadn="{$templatedir}/rss_headers.txt";
$rssentryn="{$templatedir}/rss_entry.txt";
$rssqueuen="{$templatedir}/rss_queue.txt";

$dbdir="db";
$dbf="{$dbdir}/256b.sqlite";

$baseURL='https://256c.popolon.org';

$entrydir="entries";

# generation mode: static or dynamic (stdout)
$genmod="dynamic";

# for staticfiles:
$tmpname="tmpindex.html";
$finalname="index.html";
?>

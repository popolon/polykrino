#!/bin/bash
if [ ! -e dumps ]; then mkdir dumps; fi
echo ".output 256b.sql
.dump
" | sqlite3 256b.sqlite
mv 256b.sql dumps/256b.sql.`/bin/date +%Y%m%d-%H%M%S -r 256b.sql`


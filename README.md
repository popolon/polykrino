# Polykrino

Lightweight website for TIC80 256bytes demos, could become more generalist
Use PHP,  tempalte and SQLite3 database for rendering, JavaScript usage limited to the strict minimum, due to TIC80 WebAssembly version constraints.

# Description

## Name

Polykrino is composed of poly, from greek πολύς (polús), "numerous" and krino, from greek κρίνω (krinō) "to discern"

## Goal

The goal is to have a light CMS to manage and show [TIC-80](https://tic80.com/) 256bytes demos (as in Demoscene).
A webassembly version coming from TIC80 HTML distribution package is used. You can download various system binaries from https://tic80.com/create and the source code (MIT license) on the NesBOX (main author) github account. You can test it at the URL https://256b.popolon.org/

To kept the CMS efficient :
* SQlite3 is used for DB, that's bigger than text, but better scalable and get the benefit of SQL complexes and multicriteria (polykrino) requests, as usage of multiple tag will be central. The database content is only the essential criterias and references, no Text blob or picture blob. texte are in pure text format and should use git for revision, not sure it will be usefull for this first case.
* Files are kept in a tree cutted by letters. In EXT4 file system, one directory is read as a linked list. This mean that if you have 50K files in a dir, you should accces to 50K pointers to have access to the last one. Dividing in tree is far more efficient. In this first revision, the few files are in the same directory. I will probably use a dir structure like Y/m/d or firstlatter/secondletter/thirdletter/files for one author. If the project is extended to multiuser the users will have the same kind of base, and the tree structure in their own directory.
* PHP is used, as today that's the fastest script langage for the web, and have enough user to keep a good security patching response. Some don't like him due to some problems when it was younger (before 5.x releases). It was already fast, is now faster and have cleaned some errors of it's childhood. It can be very fast if used for this best goal, small scripts for the web that do light DB request and fill templates into a light HTML page
* No remote GAFAM/NSA/WTF depencies, privacy is not negociable. Please use for your own security, when you surf on the web *uBlock Origin* to block all this crap advertising war machine and NoScript to reduce private information leaks.
* Javascript is used only for things that can't be done without it. Launching/stopping WebAssmebly version of TIC80 and communicate between user interface and TIC80. JavaScript is very slow and should be avoided if possible. It have lot of non-partability issues, as Java. Sadly that's the default script language of the Web.
* Templates are kept simple and use [TAG] kind balises. A prefix could be added in future releases to avoid collision with HTML content.
* variables are tested to avoid SQL injections or security holes everything is passed in GET format to allow user to bookmark/share what they need.

## What it does today

* Warning, it is usable like this, but this is a very developing start/tests version, API is not stable at all. I should change DB/structure. I will try to add SQL files for DB changes but this is not sure at all.
* Read datas in SQLlite3 db, templates, mix them to produce Webpage, some accessibiltiy to criteria are just at minimul (aka with bigger text).
* Tags: The system already manage several criterias for a selection, the interface is still limited to select this criterions. You can click on a tag to see all entries with a tag, and delete tags, you can add tags manually in the url with the format : tag=tag1,tag2,...
* Id: the id of a specific TIC-80 cart can be choosen by it's title, a button allow to remove it from URL and retourn then to a full list, an option of keeping previous choices (cookies ?), for now, no cookies are used.
* An iframe is used due to how is made WebASsembly+JS package of TIC80 and JavaScript to start/stop it
* A big orange Warning button is here to stop current executed TIC80 screen, Can be usefull if JavaScript+WebAssembly exhaust your system CPU ressources.
* Automatic compûtation of code length (from a text file)
* You can download cards from the interface (floppy disk icon)


## So how to add content?
* Download one of the card
* change the centent with your code, keep it in a text file (.txt) beside, if possible in UNIX or Mac format (not Microsoft one that is heavier), save it with the console mode : save filename.tic
* take a screenshot with F7 key during game, and export it with: export screen picture.png (git version) or export cover picture.gif (<=0.80) then convert it with Imagemagick for example from GIF to PNG.
* put the 3 files in the entries/ directory
* add the fields in DB using command line SQL as follow or phpliteadmin (don't put it on a public service, and use it with noscript, it uses external scripts from cloudflare.com, rawgit.com and  phpliteadmin.org.

# Installation

clone git repo and insert db:

    git clone https://framagit.org/popolon/polykrino.git
    sqlite3 polykrito/db/256b.sqlite < polykrito/db/256b.sql

You don't need write privilege frome the web server to run it. you can push content with SSH and update the SQlite base the sameway.

You need to have a Webserver ready to execute php script and make the polykrito directoy as public base. Forbid download of db/ directory and .inc file as a global practise,, there is no password/sensible information.

PHP dependances: php (tested with php-7 and php-8), php-sqlite3.

RSS feed is managed by calling:

    php updatecss.php >feed.rss

Dump script can be called in crontab as following, it create a subdir dumps if it doesn't exists, and put dumps with date+time inside

   00 00 * * * <pathtosite>/db/dumps.sh

That's all.

# Futur

## Very short term futur

* Tree structure for entries content
* Script to extract last 0x0a (UNiX LF)) character from text file using dd bs=(current size-1) count=1 and to optimize png with optipng
* Combobox to select more tags in the top navigationbar

## Short term futur
* A friendly user interface for adding content (text+card+capture)
* A multingual system (translations of interface only at first step)
* Replace/update card
* Extract the code from the card or create card from the code?

## Longer term futur
* Simple blog system ?
* Multilingual content (be able to write the same content in several languages), ans this sould be managed in core.
* Perhaps a system to create screen capture or short video capture????
* More ideas will come

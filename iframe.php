<?php
$templatedir="templates";
$iframen="{$templatedir}/iframe.txt";

$name = htmlspecialchars($_GET['name']);

if (!ctype_alnum(str_replace(array('-', '_'), '', $name))) {
  exit(-1);
}
$iframeH= fopen($iframen,'r');
$iframe = fread($iframeH, filesize($iframen));
fclose ($iframeH);
$iframe = mb_ereg_replace('\[NAME\]', $name, $iframe);
echo $iframe;
?>

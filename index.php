<?php
include "config.inc";


$tagr="";
$idr="";
$navtag="";
if ( isset($_GET['id'])) {
  $chid = htmlspecialchars($_GET['id']);
  if (ctype_digit($chid)) {
     $idr="WHERE id=$chid"; 
     $selreq = "SELECT title FROM entries {$idr}";
     $db = new SQLite3($dbf,SQLITE3_OPEN_READONLY);
     $state = $db->prepare($selreq);
     $result = $state->execute();
     $lin = $result->fetchArray(SQLITE3_ASSOC);
     $navtag="<div class=\"id\">{$lin['title']}</div><div class=\"id\"><a href=\"?\">List</a></div>";
     $db->close();
  }
}

if ( isset($_GET['tag'])) {
  $chtag = htmlspecialchars($_GET['tag']);
  if (ctype_alnum(str_replace(array(',','_'), '', $chtag))) {
    $tags=explode(',',$chtag);
    foreach ($tags as &$value) {
      if ($value != "" ) {
        if ($tagr == "") { $tagr = "tag=\"{$value}\""; }
        else { $tagr = $tagr." OR tag=\"{$value}\""; }
        $navtag= $navtag."<div class=\"tag\">${value}</div>";
      }
    } $navtag= $navtag."<div class=\"tag\"><a href=\"?tag=\">Remove tags</a></div>";
  }
}
if ($idr != "") {
 $selreq = "SELECT id,title,name FROM entries {$idr}";
} elseif ($tagr != "") {
#SELECT t1.* FROM Table1 t1, Table2 t2 WHERE t1.product_id = t2.product_id AND t2.active = TRUE
  $selreq = "SELECT DISTINCT id,title,name FROM entries t1, tags t2 WHERE t1.id = t2.entry AND ({$tagr}) GROUP BY t1.id LIMIT 10";
} else {
  $selreq = "SELECT id,title,name FROM entries LIMIT 10";
}

#echo "tagr : {$tagr}<br>\n";
#$tmpH = fopen($tmpname, "w");
//$tmpH = fopen('php://stdout', 'w');

$headH = fopen($headn, "r");
if ($headH) {
  while (($buffer = fgets($headH,4096)) !== false ) {
  $buffer = mb_ereg_replace("\[NAVTAG\]", $navtag, $buffer);
#    fwrite( $tmpH, $buffer);
    echo $buffer;
  }
  fclose($headH);
}

$db = new SQLite3($dbf,SQLITE3_OPEN_READONLY);
$state = $db->prepare($selreq);
$result = $state->execute();

while($lin = $result->fetchArray(SQLITE3_ASSOC)) {
  $codeH = fopen("{$entrydir}/{$lin['name']}.txt",'r');
  $length = filesize("{$entrydir}/{$lin['name']}.txt");
  $code = fread($codeH, $length);
  fclose ($codeH);
  $code = mb_ereg_replace('&', '&amp;', $code);
  $code = mb_ereg_replace('<', '&lt;', $code);
  $code = mb_ereg_replace('>', '&gt;', $code);
  $tagstring="";
  $tagstat = $db->prepare("SELECT tag FROM tags WHERE entry={$lin['id']} LIMIT 10");
  $tagres = $tagstat->execute();
  while($tgln = $tagres->fetchArray(SQLITE3_ASSOC)) { $tagstring = $tagstring."<div class=\"tag\"><a href=\"?tag={$tgln['tag']}\">{$tgln['tag']}</a></div>"; }
  $entryH = fopen ($entryn, "r");
  $entry = fread ($entryH, filesize ($entryn));
  $entry = mb_ereg_replace("\[TITLE\]", "<a class=\"id\" href=\"?id={$lin['id']}\">{$lin['title']}</a>", $entry);
  $entry = mb_ereg_replace("\[NAME\]", $lin['name'], $entry);
  $entry = mb_ereg_replace("\[CODE\]", $code, $entry);
  $entry = mb_ereg_replace("\[LENGTH\]", $length, $entry);
  $entry = mb_ereg_replace("\[DOWN\]", "<a href=\"{$entrydir}/{$lin['name']}.tic\">💾</a>", $entry);
  $entry = mb_ereg_replace("\[TAGS\]", $tagstring, $entry);
  #fwrite( $tmpH, $entry);
  echo $entry;
}
$db->close();

$queueH = fopen($queuen, "r");
if ($queueH) {
  while (($buffer = fgets($queueH,4096)) !== false ) {
    #fwrite( $tmpH, $buffer);
    echo $buffer;
  }
  fclose($queueH);
}
#fclose($tmpH);

//rename( $tmpname, $finalname);

?>

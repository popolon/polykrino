var Module;

function StopAll() {
  // replace last iframe by listener if it exists
  var iFrames = document.getElementsByClassName("gameIFrame");
  for (var i = 0; i < iFrames.length; i++) {
    var gf = iFrames[i].parentNode;
    //console.log("iFrame['+i']:"+iFrames[i].id);
    var gp = document.createElement("div");
    gp.setAttribute('class','game-pic');
    gf.appendChild(gp);
    gp.insertAdjacentHTML('beforeend', '<img width="100%" height="100%" class="pixelated" src="/entries/'+gf.id+'.png" title="'+gf.id+'">');
    gp.insertAdjacentHTML('beforeend', '<img class="clkpic" src="/rsc/play.svg" />');
    gf.parentNode.addEventListener("click", SetGameIFrame);
    iFrames[i].parentNode.removeChild(iFrames[i]);
  }
}

function SetGameIFrame(evt) {
 StopAll();

 // replace pic by iframe on selected node
  var gf=evt.target.parentNode.parentNode;
 // remove the listner to avoid garbage creation
  gf.parentNode.removeEventListener("click", SetGameIFrame);
  gf.insertAdjacentHTML('beforeend', '<iframe class="gameIFrame" src="/iframe.php?name='+gf.id+'" />');
  var iframe = gf.getElementsByClassName('gameIframe')[0];
  var gp = gf.getElementsByClassName("game-pic")[0];
  gp.remove();
}

const stop = document.getElementById("Stop");
stop.addEventListener("click", StopAll);

const Fiches = document.getElementsByClassName('fiche')
for (var i = 0; i < Fiches.length; i++) {
  Fiches[i].getElementsByClassName('game')[0].addEventListener("click", SetGameIFrame);
  Fiches[i].getElementsByClassName('code')[0].insertAdjacentHTML('afterbegin', '<b>'+Fiches[i].id+'</b>');
}


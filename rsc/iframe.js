  var canvasItems = document.getElementsByClassName("canvasItem");
  var fiche = document.getElementsByClassName('fiche')[0];
  var Module = {canvas: fiche.getElementsByTagName("canvas")[0], arguments:['/entries/'+fiche.id+'.tic']};

  const gameFrame = document.getElementById('game-frame');
  const displayStyle = window.getComputedStyle(gameFrame).display;

  let scriptTag = document.createElement('script'), // create a script tag
  firstScriptTag = document.getElementsByTagName('script')[0]; // find the first script tag in the document
  scriptTag.src = '/rsc/tic80.js'; // set the source of the script to your script
  firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag); // append the script to the DOM
  gameFrame.remove()
